// /prte-web-components/prte-deck/prte-deck.js
import { pieceDeckEndpt, prteNamespace } from '../constants.js'

function createElementPrte(tagName) {
	return document.createElementNS(prteNamespace, `prte:${tagName}`)
}

let deckTemplate = document.createElement('template')
const deckTemplateId = 'template_prte-deck'
/* 	const response = await fetch(
		new URL('./prte-deck-template.xhtml', import.meta.url)
	)
	const markup = await response.text()
	deckTemplate.innerHTML = markup
	deckTemplate.id = 'template_prte-deck' */
// deckTemplate.setAttributeNS('http://www.w3.org/2000/xmlns/', 'xmlns:prte', prteNamespace)
deckTemplate.id = deckTemplateId
deckTemplate.innerHTML = `
<link rel="stylesheet" href="/prte-web-components/prte-deck/prte-deck-style.css"/>
`

let deck = createElementPrte('deck')
deck.setAttributeNS('http://www.w3.org/2000/xmlns/', 'xmlns:prte', prteNamespace)

deckTemplate.append(deck)
document.head.append(deckTemplate)

class PrteDeck extends HTMLElement {
	constructor() {
		super()
		this.target = this.hasAttribute('target') ? this.getAttribute('target') : pieceDeckEndpt
	}
	
	async piecesPromise() {
		const response = await fetch(this.target)
		const pieces = await response.json()
		return pieces
	}
	async connectedCallback() {
		let deckTemplate = document.getElementById(deckTemplateId)
		this.append( deckTemplate.content.cloneNode(true) )
		// let thisDeck = this.querySelector('.deck-block')

		const pieces = await this.piecesPromise()
		const cards = pieces.map( (piece, index) => {
			let card = createElementPrte('card')
			card.setAttribute('role', 'listitem')
			const labelId = `a_card-titletext-${index}`
			card.setAttribute('aria-labelledby', labelId)

			let titleElt = createElementPrte('title')
			let titleLink = document.createElement('a')
			titleLink.href = `/pieces/${piece.piece_slug.value}`
			titleLink.innerText = piece.piece_title.value
			titleLink.classList.add('titletext')
			titleLink.id = labelId
			titleElt.append(titleLink)
			
			let creatorsElt = createElementPrte('creators')
			let mainCreatorElt = createElementPrte('main_creator')
			// innerText doesn’t seem to work on pure XML elements
			mainCreatorElt.textContent = piece.creator_name.value
			creatorsElt.append(mainCreatorElt)
			
			let thumbnailElt = createElementPrte('thumbnail')
			let thumbnailImg = document.createElement('img')
			thumbnailImg.src = `${piece.image_IRI.value}?tr=w-720`
			thumbnailImg.srcset = [
				`${piece.image_IRI.value}?tr=w-480 480w`,
				`${piece.image_IRI.value}?tr=w-720 720w`,
			].join(', ')
			thumbnailImg.alt = piece.image_alt_text.value
			thumbnailElt.append(thumbnailImg)
			
			let subtitleElt = createElementPrte('subtitle')
			subtitleElt.textContent = piece.piece_subtitle.value
			card.append(titleElt, creatorsElt, thumbnailElt, subtitleElt)

			return card
		})

		this.append(...cards)
	}
}

customElements.define('prte-deck', PrteDeck)