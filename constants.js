export {
	pieceDeckEndpt,
	prteNamespace
}

/**
 * `prteNamespace` is the XML namespace IRI for ProvocaTeach’s Web Component XML elements.
 */
const prteNamespace = 'https://xml.provocateach.art/prte-web-components'

const pieceDeckEndpt = '/api/pieces/deck/new'