// /prte-web-components/prte-navigation/prte-navigation.js

/**
 * @returns a Promise resolving to the template for this component
 */

async function template() {
	let navTemplate = document.createElement('template')
	const response = await fetch(
		new URL('./prte-navigation-template.html', import.meta.url)
	)
	const markup = await response.text()
	navTemplate.innerHTML = markup
	navTemplate.id = 'template_prte-navigation'
	return navTemplate
}

const navTemplate = await template()
document.body.append(navTemplate)

export class PrTeNavigation extends HTMLElement {
	constructor() {
		super()
		let shadow = this.attachShadow({ mode: 'open' })
		let template = document.getElementById('template_prte-navigation')
		shadow.append( template.content.cloneNode(true) )
	}
}

customElements.define('prte-navigation', PrTeNavigation)